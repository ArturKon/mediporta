﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mediporta.Models;
using Mediporta.Services;
using System.Threading;

namespace Mediporta.Controllers
{
    public class HomeController : Controller
    {       
        TagViewModel ViewModel;
        IStackOverflowService _stackOverflowService;

        public HomeController(IStackOverflowService stackOverflowService)
        {
            // DI
            ViewModel = new TagViewModel();
            _stackOverflowService = stackOverflowService;
        }

        /// <summary>
        /// Pobieranie listy tagów i zwrócenie ich w postaci tabeli.
        /// </summary>
        /// <param name="token">Token przerwania Requestów.</param>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            try
            {
                ViewModel.Status = true;
                ViewModel.Tags = await _stackOverflowService.GetTagsAsync(1000);
            }
            catch
            {
                ViewModel.Status = false;
            }

            return View(ViewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
