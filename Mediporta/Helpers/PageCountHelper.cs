﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mediporta.Helpers
{
    public static class PageCountHelper
    {
        /// <summary>
        /// Wyliczenie potrzebnej liczby stron tagów do pobrania.
        /// </summary>
        /// <param name="numberOfTags">Liczba żądanych tagów.</param>
        /// <returns>Liczba potrzebnych stron.</returns>
        public static int GetPagesCount(int numberOfTags)
        {
            return (int)Math.Ceiling(numberOfTags / 100.0);
        }
    }
}
