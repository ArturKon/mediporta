﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mediporta.Models
{
    public class TagViewModel
    {
        /// <summary>
        /// Lista tagów.
        /// </summary>
        public List<Tag> Tags { get; set; }

        /// <summary>
        /// Status żądania.
        /// </summary>
        public bool Status { get; set; }
    }
}
