﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mediporta.Models
{
    /// <summary>
    /// Model pojedynczego Taga - tylko 2 property, reszta nie jest potrzebna przy deserializacji.
    /// </summary>
    public class Tag
    {
        /// <summary>
        /// Nazwa taga.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Liczba wystąpień (użyć) taga.
        /// </summary>
        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }

        /// <summary>
        /// Liczba wystąpień (użyć) tagu w % dla danego zbioru.
        /// </summary>
        public decimal? Percentage { get; set; }
    }

    /// <summary>
    /// Model Response'a dla URL /2.2/tags - tylko 1 property, reszta nie jest potrzebna przy deserializacji.
    /// </summary>
    public class TagResponseModel
    {
        /// <summary>
        /// Lista tagów.
        /// </summary>
        [JsonProperty("items")]
        public List<Tag> Tags { get; set; }
    }
}
