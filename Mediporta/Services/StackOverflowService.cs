﻿using Mediporta.Helpers;
using Mediporta.Models;
using Newtonsoft.Json;
//using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Mediporta.Services
{
    public interface IStackOverflowService
    {
        Task<List<Tag>> GetTagsAsync(int numberOfTags);
    }

    public class StackOverflowService : IStackOverflowService
    {
        IHttpClientFactory _clientFactory;

        public StackOverflowService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        /// <summary>
        /// Pobieranie listy tagów.
        /// </summary>
        /// <param name="page">Nr strony, default = 1.</param>
        /// <returns>lista tagów / null</returns>
        private async Task<TagResponseModel> GetTagsPageAsync(int? page = 1)
        {
            if (page < 1)
                throw new ArgumentOutOfRangeException("Podano nieprawidłowy nr strony.");

            using (var client = _clientFactory.CreateClient("StackOverflowClient"))
            {
                // wywołanie Request'a
                var response = await client.GetAsync($"/2.2/tags?site=stackoverflow&page={page}&pagesize=100");

                if (response.IsSuccessStatusCode)
                {
                    // pobranie danych z Response'a
                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<TagResponseModel>(content);
                }
                else
                {
                    throw new Exception("Response code != OK");
                }
            }
        }

        /// <summary>
        /// Pobranie żądanej liczby tagów.
        /// </summary>
        /// <param name="numberOfTags">Liczba żądanych tagów.</param>
        /// <returns></returns>
        public async Task<List<Tag>> GetTagsAsync(int numberOfTags)
        {
            if (numberOfTags < 1)
                throw new ArgumentOutOfRangeException("Podano nieprawidłową liczbę żądanych tagów.");

            // wyliczenie potrzebnej liczby stron
            var pages = PageCountHelper.GetPagesCount(numberOfTags);

            // przygotowanie wątków
            var tasks = Enumerable.Range(1, pages).Select(x => GetTagsPageAsync(x));

            // uruchomienie wątków w celu asynchronicznego uzyskania wyników
            var results = await Task.WhenAll(tasks);

            var tempList = results.SelectMany(x => x.Tags);

            var countSum = tempList.Sum(x => x.Count);
            
            foreach (var tag in tempList)
            {
                tag.Percentage = decimal.Round(tag.Count / (decimal)countSum, 5);
            }

            // sortowanie wyników - nie wiadomo w jakiej kolejności przyjdą wyniki
            return tempList.OrderByDescending(x => x.Count).ToList();
        }
    }
}
