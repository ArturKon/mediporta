﻿using Mediporta.Helpers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mediporta.Tests
{
    [TestFixture]
    public class PageCountHelperTest
    {
        [Test]
        public void GetPagesCount_Given999Elements_ShouldReturn10()
        {
            var actual = PageCountHelper.GetPagesCount(999);
            Assert.AreEqual(10, actual);
        }

        [Test]
        public void GetPagesCount_Given1000Elements_ShouldReturn10()
        {
            var actual = PageCountHelper.GetPagesCount(1000);
            Assert.AreEqual(10, actual);
        }

        [Test]
        public void GetPagesCount_Given1001Elements_ShouldReturn11()
        {
            var actual = PageCountHelper.GetPagesCount(1001);
            Assert.AreEqual(11, actual);
        }
    }
}
